package org.isme.bmicalculator;

public class Calculator {
	
	private int weight;
	private int height;
	private double bmi;
	private String category;
	
	public Calculator (int weight, int height){
		if(weight<0){
			throw new IllegalArgumentException("weight should be positive!");
		}
		if(height<0){
			throw new IllegalArgumentException("height should be positive, and given in meters!"); //1 meter is 40 inch and 100 centimeters, so if its between 0 and 3, its good
		}
		if(height>3){
			throw new IllegalArgumentException("height should be positive, and given in meters!"); //the same as before
		}
		this.weight = weight;
		this.height = height;
	}
	
	public double getBMI(){
		this.bmi = weight/(height*height);
		return this.bmi;
	}
	
	public String getBmiLevel(){
		this.bmi = weight/(height*height);
		if(bmi<16){
			this.category = "Severe Thinness";
		}else if(bmi>=16 && bmi<=17){
			this.category = "Moderate Thinness";
		}else if(bmi>17 && bmi<=18.5){
			this.category = "Mild Thinness";
		}else if(bmi>18.5 && bmi<=25){
			this.category = "Normal";
		}else if(bmi>25 && bmi<=30){
			this.category = "owerweight";
		}else if(bmi>30 && bmi<=35){
			this.category = "Obese Class I";
		}else if(bmi>35 && bmi<=40){
			this.category = "Obese Class II";
		}else if(bmi>40){
			this.category = "Obese Class III";
		}
		return this.category;
	}
}
