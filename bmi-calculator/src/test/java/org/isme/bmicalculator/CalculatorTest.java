package org.isme.bmicalculator;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;


public class CalculatorTest{
	
	private Calculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator = new Calculator(80,2);
	}
	
	@Test
	public void testAddShouldCalculateMyBmiWithIntegerResult(){
		double result = calculator.getBMI();
		String categoryTest = calculator.getBmiLevel();
		assertEquals(20, result, 0);
		assertThat(categoryTest, equalTo("Normal"));
	}

}
